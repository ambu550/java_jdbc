CREATE
DATABASE IF NOT EXISTS test;


CREATE TABLE IF NOT EXISTS test.test_1
(
    id        INT(6),
    name      VARCHAR(20),
    full_name VARCHAR(100)
);

CREATE IF NOT EXISTS TABLE test.test_2
(
    id        INT(6),
    name      VARCHAR(20)
);

INSERT INTO test.test_1 (id, name, full_name)
VALUES (1, 'test1', 'TEST1'),
       (2, 'test2', 'TEST2'),
       (3, 'test3', 'TEST3');