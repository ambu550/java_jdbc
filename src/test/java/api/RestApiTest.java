package api;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static io.restassured.RestAssured.given;

import test_utils.api.RestUtils;
import test_utils.api.AssetHelper;
import io.restassured.response.Response;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class RestApiTest {

    private final static String URL = "https://reqres.in";

    @Test
    public void getUrlBasic() {

        String URL = "https://reqres.in";
        String Path = "/api/users/2";
        int StatusCode = 200;


        given()
                .filter(new AllureRestAssured())
                .baseUri(URL)
                .basePath(Path)
                .contentType(ContentType.JSON)
                .when().get()
                .then()
                .statusCode(StatusCode)
                .body("data.id", equalTo(2))
                .body("data.first_name", equalTo("Janet"))
                .body("data.last_name", equalTo("Weaver"));
    }

    @Test
    @DisplayName("Different types of Asserts")
    public void getUrlNotHamcrest() {

        String URL = "https://reqres.in";
        String Path = "/api/users/2";


        RestUtils restUtils = new RestUtils();
        AssetHelper assetHelper = new AssetHelper();

        Response response = restUtils.get(URL + "/api/users/2");

        int statusCode = response.statusCode();
        assertEquals(statusCode, 200);
        Assert.assertEquals(statusCode, 200);
    }

    @Test
    @DisplayName("Demo api check with assetHelper.AllKeyAssertString")
    public void getUrlNew() {
        RestUtils restUtils = new RestUtils();
        AssetHelper assetHelper = new AssetHelper();

        Response response = restUtils.get(URL + "/api/users/2");

        // response.prettyPrint();
        // System.out.println(response.body().asString());

        response.then().statusCode(200);
        response.then().body("data.id", equalTo(2));
        assetHelper.KeyAssertString(response, "data.first_name", "Janet");
        assetHelper.KeyAssertString(response, "data.last_name", "Weaver");

        // OR
        assetHelper.AllKeyAssertString(response, new String[][]{
                {"data.first_name", "Janet"},
                {"data.last_name", "Weaver"}
        });
    }
}
