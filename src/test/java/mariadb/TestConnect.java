package mariadb;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import io.qameta.allure.*;
import jdk.jfr.Description;
import test_utils.bd.MariaDdHelper;
import test_utils.bd.DbAssertHelper;
import mariadb.intefaces.SmokeJDBC;


@SmokeJDBC
//@SystemProperty(name = "allure.link.issue.pattern", value = "https://example.org/issue/{}")
public class TestConnect {

    @BeforeAll
    @Description("Create dump")
    public static void createDump() {
        System.out.println("Create dump before all test");
        var dbHelper = new MariaDdHelper();
        dbHelper.createDump();
    }

    @BeforeEach
    public void cleanUpBeforeEachTest() {
        System.out.println("Cleanup before each test");
        var dbHelper = new MariaDdHelper();
        dbHelper.cleanDb(new String[]{"test.test_1", "test.test_2"});
    }

    @Test
    @Issue("MB-1")
    @DisplayName("Count in empty tables, because before test work cleanup()")
    @Attachment(value = "Browser information", type = "text/plain")
    void testClean() {
        var dbAssertHelper = new DbAssertHelper();

        dbAssertHelper.assertCountInDatabase("test.test_1", 0);
        dbAssertHelper.assertCountInDatabase("test.test_2", 0);
    }

    @Test
    @Issue("MB-2")
    @DisplayName("Simple INSERT and count rows in tables")
    @Severity(SeverityLevel.CRITICAL)
    @Tag("test1")
    void testCount() {
        var dbHelper = new MariaDdHelper();
        var dbAssertHelper = new DbAssertHelper();

        String query =
                "INSERT INTO test.test_1 (id, name, full_name)\n" +
                        "VALUES ('1', 'test1', 'TEST1'),\n" +
                        "       ('2', 'test2', 'TEST2'),\n" +
                        "       ('3', 'test3', 'TEST3');";
        dbHelper.executeMutation(query);

        dbAssertHelper.assertCountInDatabase("test.test_1", 3);
    }

    @Test
    @Issue("MB-3")
    @DisplayName("Demo version of insertHelper")
    void someDbInsertSeveralRows() {
        var dbHelper = new MariaDdHelper();
        var dbAssertHelper = new DbAssertHelper();

        dbHelper.insertHelper(
                "test.test_1",
                new String[]{"id", "name", "full_name"},
                new String[][]{
                        {"1", "test1", "TEST1"},
                        {"2", "test2", "TEST2"}
                });

        dbAssertHelper.assertCountInDatabase("test.test_1", 2);
    }

    @Test
    @Issue("MB-3")
    @DisplayName("Demo of Db contains by rows conditions")
    void dbContainsByConditions() {
        var dbHelper = new MariaDdHelper();
        var dbAssertHelper = new DbAssertHelper();

        dbHelper.insertHelper(
                "test.test_1",
                new String[]{"id", "name", "full_name"},
                new String[][]{
                        {"1", "test1", "TEST1"},
                });

        //check that data is present by conditions
        dbAssertHelper.seeInDatabase("test.test_1", new String[][]{
                {"id", "1"},
                {"name", "test1"}
        });

    }

    @Test
    @Issue("MB-4")
    @DisplayName("Demo of Db DOES NOT contains rows by conditions")
    void dbDoesNotContainsByConditions() {
        var dbHelper = new MariaDdHelper();
        var dbAssertHelper = new DbAssertHelper();

        dbHelper.insertHelper(
                "test.test_1",
                new String[]{"id", "name", "full_name"},
                new String[][]{
                        {"1", "test1", "TEST1"},
                });

        //check that data is present by conditions
        dbAssertHelper.dontSeeInDatabase("test.test_1", new String[][]{
                {"id", "1"},
                {"name", "test2"}
        });

    }

}
