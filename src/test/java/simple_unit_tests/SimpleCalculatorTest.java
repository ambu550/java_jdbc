package simple_unit_tests;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleCalculatorTest {

    @Test
    void twoPlusTwoEqualFour() {
        var calculator = new SimpleCalculator();
        assertEquals(4, calculator.add(2, 2));
    }

    @Test
    void threePlusSevenEqualFour() {
        var calculator = new SimpleCalculator();
        assertEquals(10, calculator.add(3, 7));
    }

    @AfterEach
    public void afterEach() {
        System.out.println("afterEach test");
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("afterALL test");
    }

    @BeforeAll
    public static void preCondition() {
        System.out.println("preCondition for all");
    }

    @BeforeEach
    public void preConditionEach() {
        System.out.println("preCondition for EACH");
    }

}