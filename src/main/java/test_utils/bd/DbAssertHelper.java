package test_utils.bd;
import io.qameta.allure.Step;

import static org.junit.jupiter.api.Assertions.*;


public class DbAssertHelper {

    /**
     * method to test count rows in table
     *
     * @param table_name String table name for count
     * @param expected_count int expected rows in table
     *
     */
    @Step("Assert rows count {expected_count} in table {table_name}")
    public void assertCountInDatabase(String table_name, Integer expected_count){
        var dbHelper = new MariaDdHelper();

        int actual_count = dbHelper.countRows(table_name);

        assertEquals(expected_count, actual_count);
    }

    /**
     * method to test is table contain data by conditions
     *
     * @param table_name String table name for count
     * @param conditions String[][2] arrays of key => value (can modify key => condition(> < !=) => value  ?)
     *
     */
    @Step("Data exist in table {table_name} by conditions")
    public void seeInDatabase(String table_name, String[][] conditions){
        var dbHelper = new MariaDdHelper();

        int actual_count = dbHelper.countByConditions(table_name, conditions);

        assertNotEquals(0, actual_count);
    }

    /**
     * method to test is table DOES NOT contain data by conditions
     *
     * @param table_name String table name for count
     * @param conditions String[][2] arrays of key => value (can modify key => condition(> < !=) => value  ?)
     *
     */
    @Step("Data DOES NOT exist in table {table_name} by conditions")
    public void dontSeeInDatabase(String table_name, String[][] conditions){
        var dbHelper = new MariaDdHelper();

        int actual_count = dbHelper.countByConditions(table_name, conditions);

        assertEquals(0, actual_count);
    }
}
