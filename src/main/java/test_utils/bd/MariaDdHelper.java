package test_utils.bd;

import io.qameta.allure.Step;

import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MariaDdHelper {
    // JDBC URL, username and password of MySQL server
    //TODO Later take to the env
    private static final String url = "jdbc:mysql://test_mariadb:3306/test";
    //for local run
    //private static final String url = "jdbc:mysql://localhost:3306/test";
    private static final String user = "root";
    private static final String password = "example";
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;


    private void disconnect() {
        //close connection ,stmt and result set here
        try {
            con.close();
        } catch (SQLException se) { /*can't do anything */ }
    }

    private void close() {
        //close connection ,stmt and result set here
        try {
            stmt.close();
        } catch (SQLException se) { /*can't do anything */ }
        try {
            rs.close();
        } catch (SQLException se) { /*can't do anything */ }
    }

    //cut char in string from end

    /**
     * Cut char in string from end
     *
     * @param s String for cut
     * @param n int - how many chars from rnd will be cut off
     */
    private static String cutN(String s, int n) {
        Pattern p = Pattern.compile("(.*)+(?=.{" + n + "})");
        Matcher m = p.matcher(s);
        if (m.find()) {
            return m.group();
        }
        return null;
    }

    /**
     * Count rows in table
     *
     * @param table_name String The table for row count.
     * @return int
     */
    @Step("Count rows in table: {table_name}")
    public int countRows(String table_name) {
        String query = "select count(*) from " + table_name;
        return this.executeQuery(query);
    }

    /**
     * Helper method for Count rows by conditions
     *
     * @param table      String The table for insert.
     * @param conditions String[] columns has value.
     * @return int
     */
    @Step("Count rows by conditions in table {table}")
    public int countByConditions(String table, String[][] conditions) {

        String condition_part = "";

        //build condition_part
        for (String[] cond : conditions) {
            String key = cond[0];
            String value = cond[1];
            condition_part += key + "='" + value + "' AND ";
        }

        //cut extra "AND"
        condition_part = cutN(condition_part, 4);

        //Good to add column name or sorting
        String query = "SELECT COUNT(*) FROM " + table + " WHERE " + condition_part;

        return this.executeQuery(query);

    }

    /**
     * method for hardcode mutation(insert, delete etc.)
     *
     * @param query The query for execute.
     */
    @Step("Execute query: {query}")
    public void executeMutation(String query) {
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);
            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing INSERT query
            System.out.println(query);
            stmt.executeUpdate(query);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            this.disconnect();
        }
    }

    /**
     * method for hardcode query(SELECT) for count now
     *
     * @param query The query for execute.
     */
    @Step("Execute query: {query}")
    public int executeQuery(String query) {
        int count = 0;
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);
            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing SELECT query
            System.out.println(query);
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                count = rs.getInt(1);
                // System.out.println("Total number in the table " + table_name + ": " + count);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            this.disconnect();
            this.close();
        }
        return count;
    }

    /**
     * Helper method for insert data
     *
     * @param table      String The table for insert.
     * @param columns    String[] columns for insert.
     * @param rows_array String[][] rows for insert.
     */
    @Step("Insert data in database")
    public void insertHelper(String table, String[] columns, String[][] rows_array) {
        //TODO think about some kind of MERGE+TEMPLATE
        //TODO think about multiple DBs
        //build column
        String column = "";
        for (String str : columns) {
            column += " " + str + ",";
        }
        column = cutN(column, 1);

        //build get sub array
        for (String[] rows : rows_array) {

            //build input
            String row = "";
            for (String str : rows) {
                row += "'" + str + "',";
            }

            //cut last char
            row = cutN(row, 1);

            //Good to add column name or sorting
            String query = "INSERT INTO " + table + "(" + column + ")" +
                    " VALUES (" + row + ");";

            this.executeMutation(query);
        }
    }

    /**
     * Helper method for clean data
     *
     * @param tables_names String[] tables for cleanup.
     */
    @Step("clean db")
    public void cleanDb(String[] tables_names) {
        for (String table_name : tables_names) {
            // executing TRUNCATE query
            //TODO ADD "ALTER TABLE table_name AUTO_INCREMENT = 1"
            String query = "TRUNCATE table " + table_name;
            System.out.println(query);

            this.executeMutation(query);
        }
    }

    /**
     * Helper method for dump data
     * hardcode for now
     */
    @Step("Create  dump")
    public void createDump() {
        this.executeMutation("CREATE TABLE IF NOT EXISTS test.test_1 \n" +
                "(\n" +
                "    id        INT(6),\n" +
                "    name      VARCHAR(20),\n" +
                "    full_name VARCHAR(100)\n" +
                ");");

        this.executeMutation("CREATE TABLE IF NOT EXISTS test.test_2\n" +
                "(\n" +
                "    id        INT(6),\n" +
                "    name      VARCHAR(20)\n" +
                ");");
    }

}
