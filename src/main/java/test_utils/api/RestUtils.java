package test_utils.api;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class RestUtils {

    /**
     * Run get request
     *
     * @param url String url for request
     */
    @Step("Run get request {url}")
    public Response get(String url) {
        return RestAssured.given()
                .filter(new AllureRestAssured())
                .when()
                .get(url);
    }

}
