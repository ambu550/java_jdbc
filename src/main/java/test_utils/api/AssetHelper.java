package test_utils.api;

import io.qameta.allure.Step;
import io.restassured.response.Response;

import static org.hamcrest.Matchers.equalTo;

public class AssetHelper {

    @Step("check key {key} value")
    //can be private only for cycle
    public void KeyAssertString(Response response, String key, String data_string) {
        response.then().body(key, equalTo(data_string));
    }

    @Step("check ALL keys value")
    public void AllKeyAssertString(Response response, String[][] assert_keys_data) {

        for (String[] extract : assert_keys_data) {
            String key = extract[0];
            String data_string = extract[1];
            this.KeyAssertString(response, key, data_string);
        }

    }

}
