## start
    docker-compose up -d
    docker exec -it tester mvn clean test
    allure generate ./target/allure-results --clean -o ./allure-report ; allure open ./allure-report

## connect
    user: root 
    password: example 
    database: test 
    port: 3306

## Demo insertHelper
<img src="screens/mariaDB_helper.png" />

## Demo cleaner
<img src="screens/db_cleaner.png" />

## Demo KeyAssertString
<img src="screens/key_assert.png" />


### local (don't forget change URL in MariaDdHelper.java)
    mvn clean test
    mvn clean test -Dtest=TestConnect
    mvn clean test -Dallure.link.issue.pattern=http://test.com/issues/{}

### in docker
    docker exec -it tester mvn clean test

## back permission to local user
    sudo chown -R  ${USER}:${USER} ./*

## local allure report generate
    allure generate ./target/allure-results --clean -o ./allure-report ; allure open ./allure-report