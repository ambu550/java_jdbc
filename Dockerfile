FROM maven:3.8.6-jdk-11

COPY . home/java_jdbc/
WORKDIR home/java_jdbc/


ENTRYPOINT ["tail", "-f", "/dev/null"]